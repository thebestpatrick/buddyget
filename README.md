# Buddyget

A budgeting app

## Build

ensure you have wasm ready:

`rustup target add wasm32-unknown-unknown`

install [trunk](https://github.com/trunk-rs/trunk):

`cargo install trunk wasm-bindgen-cli`

build normally:

`cargo build`

serve with trunk:

`trunk serve --open`