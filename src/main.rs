use yew::prelude::*;
use std::{fmt, time::{Duration, SystemTime}};

enum Msg {
    AddExpense,
    DelExpense,
}

#[derive(Debug)]
enum Feedback {
    Error(String),
    Warning(String),
    Info(String),
    Debug(String),
}

#[derive(Debug)]
struct Expense {
    amount: usize,
    created: f64,
    timestamp: f64,
}

impl fmt::Display for Expense {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.amount)
    }
}

struct App {
    expenses: Vec<Expense>,
}

impl App {
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            expenses: Vec::new(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::AddExpense => {
                true
            },
            Msg::DelExpense => {
                true
            },
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <main>
              <div>
                <h1>{ "Welcome" }</h1>
                <p>{ "budget stuff goes here..." }</p>
              </div>
            </main>
        }
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
